package br.com.itau.investimentos.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class RetornoSimulacao {

    @JsonProperty("investimento")
    private String nomeInvestimento;

    @JsonProperty("valor_investido")
    private BigDecimal valorInvestido;

    @JsonProperty("valor_final")
    private BigDecimal valorFinal;

    @JsonProperty("prazo_em_meses")
    private int prazoMeses;

    @JsonProperty("mensagem")
    private String mensagem;

    public String getNomeInvestimento() {
        return nomeInvestimento;
    }

    public void setNomeInvestimento(String nomeInvestimento) {
        this.nomeInvestimento = nomeInvestimento;
    }

    public BigDecimal getValorFinal() {
        return valorFinal;
    }

    public void setValorFinal(BigDecimal valorFinal) {
        this.valorFinal = valorFinal;
    }

    public int getPrazoMeses() {
        return prazoMeses;
    }

    public void setPrazoMeses(int prazoMeses) {
        this.prazoMeses = prazoMeses;
    }

    public BigDecimal getValorInvestido() {
        return valorInvestido;
    }

    public void setValorInvestido(BigDecimal valorInvestido) {
        this.valorInvestido = valorInvestido;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public RetornoSimulacao(String nomeInvestimento, BigDecimal valorFinal, BigDecimal valorInvestido, int prazoMeses) {
        this.nomeInvestimento = nomeInvestimento;
        this.valorFinal = valorFinal;
        this.valorInvestido = valorInvestido;
        this.prazoMeses = prazoMeses;

        this.mensagem = "Após " + this.prazoMeses +
                " meses, seu investimento de R$ " + this.valorInvestido +
                " irá se converter em R$ " + this.valorFinal;
    }
}
