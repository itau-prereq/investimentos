package br.com.itau.investimentos.DTO;

import br.com.itau.investimentos.models.Investimento;
import br.com.itau.investimentos.models.Lead;
import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.*;
import java.util.List;


public class InvestimentoDTO {
    @NotBlank(message = "Nome do investimento precisa vir preenchido")
    @Size(min = 3, message = "Nome muito curto")
    private String nome;

    @NotNull(message = "Rendimento precisa vir preenchido")
    @DecimalMin(value =  "0.01", message = "Rendimento minimo 0.01")
    private Double rendimento;

    public InvestimentoDTO() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getRendimento() {
        return rendimento;
    }

    public void setRendimento(Double rendimento) {
        this.rendimento = rendimento;
    }


    public Investimento convertToInvestimento(){
        Investimento investimento = new Investimento();
        investimento.setNome(this.nome);
        investimento.setRendimento(this.rendimento);

        return investimento;
    }
}
