package br.com.itau.investimentos.DTO;

import br.com.itau.investimentos.models.Lead;
import br.com.itau.investimentos.models.Simulacao;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

public class LeadDTO {

    //@Column(name = "nome_completo") //Sobrescreve o nome da tabela
    @NotBlank(message = "Nome deve vir preenchido")//Nao permite branco
    @Size(min = 3, message = "Nome veio em branco")//Nao permite tamanho menor que 3
    @JsonProperty("nome_interessado")
    private String nome;

    @NotNull(message = "Prazo desejado deve vir preenchido")//Nao permite null
    @JsonProperty("quantidade_meses")
    private int prazo;

    @Email(message = "Email invalido")
    @NotNull(message = "Email veio como null")//Nao permite null
    private String email;

    @NotNull(message = "Valor deve vir preenchido")//Nao permite null
    @JsonProperty("valor_aplicado")
    private BigDecimal valorInvestido;

    public LeadDTO(){

    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getPrazo() {
        return prazo;
    }

    public void setPrazo(int prazo) {
        this.prazo = prazo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public BigDecimal getValorInvestido() {
        return valorInvestido;
    }

    public void setValorInvestido(BigDecimal valorInvestido) {
        this.valorInvestido = valorInvestido;
    }

    public Lead converterParaLead(){
        Lead lead = new Lead();
        lead.setNome(this.nome);
        lead.setEmail(this.email);

        return lead;
    }

    public Simulacao converterParaSimulacao(){
        Simulacao simulacao = new Simulacao();
        simulacao.setMeses(this.prazo);
        simulacao.setValorInvestido(this.valorInvestido.doubleValue());

        return simulacao;
    }
}
