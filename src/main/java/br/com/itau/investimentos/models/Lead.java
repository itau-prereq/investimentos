package br.com.itau.investimentos.models;

import org.hibernate.validator.constraints.br.CPF;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Entity
public class Lead{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    //@Column(name = "nome_completo") //Sobrescreve o nome da tabela
    @NotBlank(message = "Nome deve vir preenchido")//Nao permite branco
    @Size(min = 3, message = "Nome veio em branco")//Nao permite tamanho menor que 3
    private String nome;

    @Email(message = "Email invalido")
    @NotNull(message = "Email veio como null")//Nao permite null
    @Column(unique = true)
    private String email;

    public Lead(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
