package br.com.itau.investimentos.models;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

@Entity
public class Simulacao  implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne(targetEntity=Lead.class)
    private Lead lead;

    @ManyToOne(targetEntity=Investimento.class)
    private Investimento investimento;

    @NotNull(message = "Valor deve vir preenchido")//Nao permite null
    @DecimalMin(value =  "0.01", message = "Rendimento minimo 0.01")
    private Double valorInvestido;

    @NotNull(message = "Valor deve vir preenchido")//Nao permite null
    @DecimalMin(value =  "0.01", message = "Rendimento minimo 0.01")
    private Double valorRetorno;

    @NotNull(message = "Valor deve vir preenchido")//Nao permite null
    @Min(value =  1, message = "Tempo minimo de investimento: 1 mes")
    private int meses;

    private LocalDate dataDaConsulta;

    public Simulacao() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Lead getLead() {
        return lead;
    }

    public void setLead(Lead lead) {
        this.lead = lead;
    }

    public Investimento getInvestimento() {
        return investimento;
    }

    public void setInvestimento(Investimento investimento) {
        this.investimento = investimento;
    }

    public Double getValorInvestido() {
        return valorInvestido;
    }

    public void setValorInvestido(Double valorInvestido) {
        this.valorInvestido = valorInvestido;
    }

    public Double getValorRetorno() {
        return valorRetorno;
    }

    public void setValorRetorno(Double valorRetorno) {
        this.valorRetorno = valorRetorno;
    }

    public int getMeses() {
        return meses;
    }

    public void setMeses(int meses) {
        this.meses = meses;
    }

    public LocalDate getDataDaConsulta() {
        return dataDaConsulta;
    }

    public void setDataDaConsulta(LocalDate dataDaConsulta) {
        this.dataDaConsulta = dataDaConsulta;
    }
}
