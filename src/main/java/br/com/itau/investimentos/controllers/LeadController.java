package br.com.itau.investimentos.controllers;

import br.com.itau.investimentos.DTO.LeadDTO;
import br.com.itau.investimentos.models.Lead;
import br.com.itau.investimentos.services.LeadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RequestMapping("/leads")
@RestController
public class LeadController {

    @Autowired
    private LeadService leadService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Lead adicionaLead(@RequestBody @Valid LeadDTO lead){
        return leadService.salvarLead(lead);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Iterable<Lead> retornaTodosLead(){
        return leadService.retornaTodosLeads();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Lead retornaLeadPorId(@PathVariable(name = "id") int id){
        try{
            return leadService.buscarLeadPorId(id);
        } catch (Exception e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Lead atualizaLead(@PathVariable(name = "id") int id, @RequestBody @Valid Lead lead){
        try{
            return leadService.atualizarLead(id, lead);
        } catch (Exception e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletaLead(@PathVariable(name = "id") int id){
        try{
            leadService.deletaLead(id);
        } catch (Exception e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}

