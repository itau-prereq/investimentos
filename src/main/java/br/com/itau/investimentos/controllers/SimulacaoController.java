package br.com.itau.investimentos.controllers;

import br.com.itau.investimentos.models.Lead;
import br.com.itau.investimentos.models.Simulacao;
import br.com.itau.investimentos.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RequestMapping("/simulacoes")
@RestController
public class SimulacaoController {
    @Autowired
    private SimulacaoService simulacaoService;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Iterable<Simulacao> retornaTodasSimulacoes(){
        return simulacaoService.retornaTodasSimulacoes();
    }

    @GetMapping("/lead/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Iterable<Simulacao> retornaSimulacoesPorLeadId(@PathVariable(name = "id") int id){
        try{
            return simulacaoService.buscarSimulacoesPorLead(id);
        } catch (Exception e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
