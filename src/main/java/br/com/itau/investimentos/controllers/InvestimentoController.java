package br.com.itau.investimentos.controllers;

import br.com.itau.investimentos.DTO.InvestimentoDTO;
import br.com.itau.investimentos.DTO.LeadDTO;
import br.com.itau.investimentos.DTO.RetornoSimulacao;
import br.com.itau.investimentos.models.Investimento;
import br.com.itau.investimentos.services.InvestimentoService;
import br.com.itau.investimentos.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/investimentos")
public class InvestimentoController {

    @Autowired
    private InvestimentoService investimentoService;

    @Autowired
    private SimulacaoService simulacaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Investimento adicionaInvestimento(@RequestBody @Valid InvestimentoDTO investimentoDTO){
        return investimentoService.salvarInvestimento(investimentoDTO);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Iterable<Investimento> retornaInvestimento(){
        return investimentoService.retornaInvestimento();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Investimento retornaInvestimentoPorId(@PathVariable(name = "id") int id){
        try{
            return investimentoService.retornaInvestimentoPorId(id);
        } catch (Exception e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Investimento atualizaInvestimento(@PathVariable(name = "id") int id, @RequestBody @Valid Investimento investimento){
        try{
            return investimentoService.atualizarInvestimento(id, investimento);
        } catch (Exception e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletaLead(@PathVariable(name = "id") int id){
        try{
            investimentoService.deletaInvestimento(id);
        } catch (Exception e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PostMapping("/{id}/simulacao")
    @ResponseStatus(HttpStatus.OK)
    public RetornoSimulacao simulacao(@PathVariable(name = "id") int idInvestimentos, @RequestBody @Valid LeadDTO leadDTO){
        return simulacaoService.simulaInvestimento(idInvestimentos, leadDTO);
    }
}