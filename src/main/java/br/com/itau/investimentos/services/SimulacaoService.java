package br.com.itau.investimentos.services;

import br.com.itau.investimentos.DTO.InvestimentoDTO;
import br.com.itau.investimentos.DTO.LeadDTO;
import br.com.itau.investimentos.DTO.RetornoSimulacao;
import br.com.itau.investimentos.models.Investimento;
import br.com.itau.investimentos.models.Lead;
import br.com.itau.investimentos.models.Simulacao;
import br.com.itau.investimentos.repositories.SimulacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class SimulacaoService {

    @Autowired
    private SimulacaoRepository simulacaoRepository;

    @Autowired
    private LeadService leadService;

    @Autowired
    private InvestimentoService investimentoService;

    public Simulacao salvarSimulacao(Simulacao simulacao) throws RuntimeException{
        return simulacaoRepository.save(simulacao);
    }

    public Iterable<Simulacao>  retornaTodasSimulacoes(){
        return simulacaoRepository.findAll();
    }

    public Iterable<Simulacao>  buscarSimulacoesPorLead(int id) throws RuntimeException {
        return simulacaoRepository.findAllByLeadId(id);
    }

    public RetornoSimulacao simulaInvestimento(int id, LeadDTO leadDTO) throws RuntimeException {

        Investimento investimento = investimentoService.retornaInvestimentoPorId(id);

        Lead lead = leadService.buscarLeadPorEmailENome(leadDTO.getEmail(), leadDTO.getNome());

        if(lead == null) {
            lead = leadService.salvarLead(leadDTO);
        }

        Simulacao simulacao = leadDTO.converterParaSimulacao();
        simulacao.setDataDaConsulta(LocalDate.now());
        simulacao.setLead(lead);
        simulacao.setInvestimento(investimento);
        simulacao.setValorRetorno(calculaRetorno(leadDTO.getPrazo(), investimento, leadDTO.getValorInvestido()).doubleValue());

        salvarSimulacao(simulacao);

        return new RetornoSimulacao(investimento.getNome(), BigDecimal.valueOf(simulacao.getValorRetorno()), leadDTO.getValorInvestido(), leadDTO.getPrazo());
    }

    public BigDecimal calculaRetorno(int meses, Investimento investimento, BigDecimal valorInvestido){

        BigDecimal valorFinal = valorInvestido;

        for( int i = 0; i < meses; i++){
            double rendimento = (investimento.getRendimento()/100) + 1;
            valorFinal = valorFinal.multiply(BigDecimal.valueOf(rendimento)).setScale(2, RoundingMode.CEILING);
        }

        return valorFinal;
    }
}
