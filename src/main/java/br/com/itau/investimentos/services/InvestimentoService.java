package br.com.itau.investimentos.services;

import br.com.itau.investimentos.DTO.InvestimentoDTO;
import br.com.itau.investimentos.models.Investimento;
import br.com.itau.investimentos.repositories.InvestimentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class InvestimentoService {

    @Autowired
    private InvestimentoRepository investimentoRepository;

    @Autowired
    private LeadService leadService;


    public Investimento salvarInvestimento(InvestimentoDTO investimentoDTO) throws RuntimeException{
        Investimento investimento = investimentoDTO.convertToInvestimento();
        return investimentoRepository.save(investimento);
    }

    public Iterable<Investimento> retornaInvestimento(){
        return investimentoRepository.findAll();
    }

    public Investimento retornaInvestimentoPorId(int id) throws RuntimeException{
        Optional<Investimento> investimentoOptional = investimentoRepository.findById(id);

        if(investimentoOptional.isPresent()){
            return investimentoOptional.get();
        } else {
            throw new RuntimeException("Investimento não encontrado.");
        }
    }

    public List<Investimento> retornaInvestimentoPorNome(String nome) throws RuntimeException{
        List<Investimento> investimentoOptional = investimentoRepository.findByNome(nome);

        if(!investimentoOptional.isEmpty()){
            return investimentoOptional;
        } else {
            throw new RuntimeException("Lead não encontrado.");
        }
    }

    public void deletaInvestimento(int id) throws RuntimeException {
        if(investimentoRepository.existsById(id)){
            investimentoRepository.deleteById(id);
        } else {
            throw new RuntimeException("Lead não existe");
        }
    }

    public Investimento atualizarInvestimento(int id, Investimento investimento) throws RuntimeException {
        Investimento investimentoDB = retornaInvestimentoPorId(id);
        investimento.setId(investimentoDB.getId());

        return investimentoRepository.save(investimento);
    }

}
