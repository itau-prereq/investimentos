package br.com.itau.investimentos.services;

import br.com.itau.investimentos.DTO.InvestimentoDTO;
import br.com.itau.investimentos.DTO.LeadDTO;
import br.com.itau.investimentos.models.Investimento;
import br.com.itau.investimentos.models.Lead;
import br.com.itau.investimentos.repositories.InvestimentoRepository;
import br.com.itau.investimentos.repositories.LeadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class LeadService {
    @Autowired
    private LeadRepository leadRepository;

    @Autowired
    private InvestimentoRepository investimentoRepository;

    public Lead salvarLead(LeadDTO leadDTO){

        Lead lead = leadDTO.converterParaLead();

        return leadRepository.save(lead);
    }

    public Iterable<Lead>  retornaTodosLeads(){
        return leadRepository.findAll();
    }

    public Lead buscarLeadPorId(int id) throws RuntimeException {
        Optional<Lead> leadOptional = leadRepository.findById(id);

        if(leadOptional.isPresent()){
            return leadOptional.get();
        } else {
            throw new RuntimeException("Lead não encontrado.");
        }
    }

    public Lead buscarLeadPorEmailENome(String email, String nome) {
        Optional<Lead> leadOptional = leadRepository.findByEmailAndNome(email, nome);

        return leadOptional.orElse(null);
    }

    public Lead atualizarLead(int id, Lead lead) throws RuntimeException {
        Lead leadDB = buscarLeadPorId(id);
        lead.setId(leadDB.getId());

        return leadRepository.save(lead);
    }

    public void deletaLead(int id) throws RuntimeException {
        if(leadRepository.existsById(id)){
            leadRepository.deleteById(id);
        } else {
            throw new RuntimeException("Lead não existe");
        }
    }
}
