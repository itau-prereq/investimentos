package br.com.itau.investimentos.repositories;

import br.com.itau.investimentos.models.Lead;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface LeadRepository extends CrudRepository<Lead, Integer> {

    Optional<Lead> findByEmail(String email);

    Optional<Lead> findByEmailAndNome(String email, String nome);

}
