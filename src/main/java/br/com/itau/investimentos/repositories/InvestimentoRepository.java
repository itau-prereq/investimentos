package br.com.itau.investimentos.repositories;

import br.com.itau.investimentos.models.Investimento;
import br.com.itau.investimentos.models.Lead;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface InvestimentoRepository extends CrudRepository<Investimento, Integer> {

    List<Investimento> findByNome(String nome);

}
