package br.com.itau.investimentos.repositories;

import br.com.itau.investimentos.models.Simulacao;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface SimulacaoRepository extends CrudRepository<Simulacao, Integer> {

    Optional<Simulacao> findByInvestimentoAndLead(int investimentoId, int leadId);

    Iterable<Simulacao> findAllByLeadId(int leadId);
}
