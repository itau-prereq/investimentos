package br.com.itau.investimentos.services;

import br.com.itau.investimentos.DTO.InvestimentoDTO;
import br.com.itau.investimentos.models.Investimento;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.Optional;

public class InvestimentoServiceTest {

    @MockBean
    private LeadService leadService;

    @MockBean
    private SimulacaoService simulacaoService;

    @MockBean
    private InvestimentoService investimentoService;

    @Autowired
    private MockMvc mockMvc;

    private Investimento investimento;

    @BeforeEach
    public void setUp() {
        this.investimento = new Investimento();
        investimento.setNome("Investimento teste");
        investimento.setRendimento(0.04);
    }

}
